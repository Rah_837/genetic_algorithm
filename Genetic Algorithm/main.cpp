#include "GenAlgApp.h"



int main()
{
	GenAlgApp app;

	app.create(sf::Style::None, {	RECTANGLE_SPACE_X + (RECTANGLE_SIZE_X + RECTANGLE_SPACE_X) * MAP_CAPACITY_X,
									RECTANGLE_SPACE_Y + (RECTANGLE_SIZE_Y + RECTANGLE_SPACE_Y) * MAP_CAPACITY_Y }, "Genetic Algorithm");

	app.run();

	return 0;
}