#include "GenAlgApp.h"



GenAlgApp::GenAlgApp()
{
	for (sf::Uint16 x = 0; x < MAP_CAPACITY_X; ++x)
	{
		for (sf::Uint16 y = 0; y < MAP_CAPACITY_Y; ++y)
		{
			auto& rect = m_map[x][y];

			rect.setFillColor(sf::Color::Green);
			rect.setSize({ RECTANGLE_SIZE_X, RECTANGLE_SIZE_Y });
			rect.setPosition({	static_cast<float>(RECTANGLE_SPACE_X + (RECTANGLE_SIZE_X + RECTANGLE_SPACE_X) * x),
								static_cast<float>(RECTANGLE_SPACE_Y + (RECTANGLE_SIZE_Y + RECTANGLE_SPACE_Y) * y) });
		}
	}
}

GenAlgApp::~GenAlgApp() {  }


void GenAlgApp::update()
{
	for (auto& arr : m_map)
		for (auto& rect : arr)
			m_window.draw(rect);
}

void GenAlgApp::eventHandler(const sf::Event &event)
{
	if (event.type == sf::Event::KeyReleased)
	{
		if (event.key.code == sf::Keyboard::Escape)
			m_window.close();
	}
}