#pragma once

#include <SFML\Graphics.hpp>



class Application
{
public:
	Application();
	virtual ~Application();

	void create(sf::Uint32 = sf::Style::Default, sf::VideoMode = sf::VideoMode::getDesktopMode(), sf::String = "Window");
	void close();

	void run();

protected:
	sf::RenderWindow	m_window;
	float				m_timeSpeed;

private:
	virtual void update() = 0;
	virtual void eventHandler(const sf::Event&) = 0;
};