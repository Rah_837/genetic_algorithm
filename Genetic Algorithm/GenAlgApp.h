#pragma once

#include "Application.h"

#include <array>

#define RECTANGLE_SIZE_X 16
#define RECTANGLE_SIZE_Y 16

#define RECTANGLE_SPACE_X 2
#define RECTANGLE_SPACE_Y 2

#define MAP_CAPACITY_X 64
#define MAP_CAPACITY_Y 32



class GenAlgApp final : public Application
{
public:
	GenAlgApp();
	~GenAlgApp();

	void update() override;
	void eventHandler(const sf::Event&) override;

	std::array<std::array<sf::RectangleShape, MAP_CAPACITY_Y>, MAP_CAPACITY_X> m_map;
};